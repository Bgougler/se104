package studentroster;

public class StudentController {
	public static void main(String arg[]) {
		StudentModel test = new StudentModel();
		for (int i = 0; i < test.getSize(); i++) {
			System.out.println("Student " + i + ":");
			System.out.println(test.getStudent(i));
			System.out.println("************************");
		}
	}
}
