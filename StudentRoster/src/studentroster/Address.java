package studentroster;

public class Address {
	String street;
	String city;
	String state;
	String zip;

	Address(String s, String c, String st, String z) {
		street = s;
		city = c;
		state = st;
		zip = z;
	}

	public String toString() {
		return "Address: " + street + ", " + city + ", " + state + " " + zip;
	}
}
