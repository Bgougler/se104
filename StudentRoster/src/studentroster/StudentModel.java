package studentroster;

import java.util.*;

public class StudentModel {
	public ArrayList<Student> students = new ArrayList<Student>();

	String studentData = "Billy,Jenkins,2/3/0366,HIS,7586 Goodland Parkway,Harrisburg,Pennsylvania,17110\nSteve,Wood,12/27/0615,CIS,651 Prairie Rose Alley,Temple,Texas,76505\nDiane,Foster,10/19/1270,CE,77 Ridgeview Alley,Punta Gorda,Florida,33982\nRachel,Arnold,7/28/0413,EN,04 Portage Terrace,Winston Salem,North Carolina,27105\nSusan,Sims,11/27/1263,MA,02 Sundown Circle,Lincoln,Nebraska,68524\nKathy,Richards,5/11/0991,SE,57958 Eastwood Avenue,Dayton,Ohio,45490\nLillian,Wallace,11/29/0289,CS,05 Jana Drive,Wichita Falls,Texas,76310\nWillie,Fox,8/5/0665,MA,1950 Amoth Center,Nashville,Tennessee,37250\nBrian,Spencer,4/26/0243,CS,002 Fallview Alley,Duluth,Georgia,30195\nNorma,Mason,3/17/1775,HIS,183 Hauk Trail,San Antonio,Texas,78250\nBenjamin,Day,8/1/1363,CE,9622 Algoma Lane,Jacksonville,Florida,32230\nLouis,Barnes,CS,2/19/0379,03 Bellgrove Road,Tyler,Texas,75799\nEugene,Moreno,9/20/1771,EN,8998 Ruskin Circle,Atlanta,Georgia,30336\nBonnie,Weaver,10/31/0806,HIS,529 Cascade Plaza,Lake Charles,Louisiana,70616\nTodd,Wheeler,12/24/1074,CE,80 Hintze Court,Tucson,Arizona,85715\nKathy,Coleman,1/15/0892,SE,4 Corry Center,Lexington,Kentucky,40510\nJudy,Pierce,10/10/0522,HIS,7640 Elgar Lane,Albuquerque,New Mexico,87195\nLois,Henry,12/6/0339,EN,7951 Esch Drive,Pittsburgh,Pennsylvania,15266\nDenise,Lopez,10/14/0664,MA,386 Farwell Trail,Jackson,Mississippi,39282\nJacqueline,Olson,9/18/0309,HIS,0680 Hoard Way,Bethesda,Maryland,20892\nRebecca,Holmes,2/28/1620,SE,040 Stoughton Crossing,El Paso,Texas,88584\nJoshua,Greene,2/3/1958,EN,0066 Ohio Center,Torrance,California,90505\nJack,Diaz,3/19/1601,CE,9457 Mccormick Street,Lake Charles,Louisiana,70607\nJerry,Reynolds,6/22/0308,EN,70058 Heath Street,Rochester,New York,14624\nRuby,Oliver,10/8/0553,MA,099 Blaine Avenue,Louisville,Kentucky,40280\nBrian,Wright,5/28/1438,SE,0 Carberry Alley,Eugene,Oregon,97405\nJimmy,Jackson,9/5/1614,CE,00277 Charing Cross Drive,Richmond,Virginia,23242\nMaria,Weaver,5/27/0479,HIS,4270 Menomonie Trail,Brockton,Massachusetts,02305\nNorma,Sims,6/5/1343,SE,789 Steensland Place,Jackson,Tennessee,38308\nAntonio,Russell,4/13/0963,CE,113 Eggendart Circle,Minneapolis,Minnesota,55428\nKathy,Garcia,9/16/0714,MA,7 Loftsgordon Circle,Amarillo,Texas,79188\nAngela,Simpson,8/22/0259,HIS,4533 John Wall Drive,Youngstown,Ohio,44555\nBenjamin,Watkins,11/16/0721,CE,56497 Drewry Crossing,Little Rock,Arkansas,72215\nKaren,Pierce,1/18/1731,CE,57 Anniversary Trail,Tulsa,Oklahoma,74126\nChristina,Banks,10/25/0299,HIS,056 Manitowish Terrace,Memphis,Tennessee,38131\nAaron,Schmidt,3/15/1672,CE,67798 Amoth Plaza,Washington,District of Columbia,20520";

	String[] tokens;
	Student aStudent;

	StudentModel() {
		String lines[] = studentData.split("\n");
		for (String line : lines) {
			tokens = line.split(",");
			aStudent = new Student(tokens[0], tokens[1], tokens[2], tokens[3],tokens[4], tokens[5], tokens[6], tokens[7]);
			students.add(aStudent);
		}
	}

	public String getStudent(int i) {
		Student s = students.get(i);
		String sInfo = s.toString();
		return sInfo;
	}

	public int getSize() {
		return students.size();
	}
}
