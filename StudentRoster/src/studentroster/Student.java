package studentroster;

public class Student {
	String firstName;
	String lastName;
	String birthDate;
	String major;
	Address home;

	public Student(String fName, String lName, String date, String maj, String s, String c, String st, String z) {
		firstName = fName;
		lastName = lName;
		birthDate = date;
		major = maj;
		home = new Address(s, c, st, z);
	}

	public String toString() {
		return "Name: " + firstName + " " + lastName + "\nBirthday: " + birthDate + "\nMajor: " + major + "\n" + home.toString();
	}
}
